import { produce } from 'immer';
import { WEBSOCKET_MACHINES_UPDATE } from '@src/contants';

const baseState = [{}];

export const updateMachinesWebsocket = produce((draft, action) => {
  switch (action.type) {
    case WEBSOCKET_MACHINES_UPDATE: {
      draft.push(action.websocketMachines);
      break;
    }
  }
}, baseState);
