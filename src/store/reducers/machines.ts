import { AnyAction } from 'redux';
import { produce } from 'immer';

import { FETCH_MACHINES_SUCCESS, UPDATE_MACHINES } from '@src/contants';

interface InitialStateProps {
  data: MachinesGlobal[];
  loading: boolean;
}

const initialState: InitialStateProps = {
  data: [],
  loading: false,
};

export const machines = produce((draft, action: AnyAction) => {
  switch (action.type) {
    case FETCH_MACHINES_SUCCESS:
      draft.data = action.machines.data.map((value: MachinesGlobal) =>
        Object.assign(value, { timestamp: 0 }),
      );
      draft.loading = false;
      break;
    case UPDATE_MACHINES: {
      const matchObjectIndex = action.data.machines.data.findIndex(
        (obj: Record<string, string | number>) =>
          obj.id === action.data.websocketMachines.machine_id,
      );
      draft.data = action.data.machines.data;
      if (matchObjectIndex !== -1) {
        draft.data[matchObjectIndex].status =
          action.data.websocketMachines.status;
        draft.data[matchObjectIndex].timestamp =
          action.data.websocketMachines.timestamp;
      }
      break;
    }
  }
}, initialState);
