import { AnyAction } from 'redux';

import { INIT_WEBSOCKET_SUCCESS } from '@src/contants';

interface InitialStateProps {
  status: boolean;
}

const initialState: InitialStateProps = {
  status: false,
};

export const initWebsocket = (
  state = initialState,
  action: AnyAction,
): InitialStateProps => {
  switch (action.type) {
    case INIT_WEBSOCKET_SUCCESS:
      return { status: true };
    default:
      return state;
  }
};
