import { AnyAction } from 'redux';

import { WEBSOCKET_MACHINES_FETCHED } from '@src/contants';

interface InitialStateProps {
  id: string;
  machine_id: string;
  status: string;
  timestamp: Date;
}

const initialState: InitialStateProps = {
  id: '',
  machine_id: '',
  status: '',
  timestamp: new Date(),
};

export const websocketMachines = (
  state = initialState,
  action: AnyAction,
): InitialStateProps => {
  switch (action.type) {
    case WEBSOCKET_MACHINES_FETCHED:
      return {
        id: action.websocketMachines.id,
        machine_id: action.websocketMachines.machine_id,
        status: action.websocketMachines.status,
        timestamp: action.websocketMachines.timestamp,
      };
    default:
      return state;
  }
};
