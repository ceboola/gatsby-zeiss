import { combineReducers } from 'redux';

import { machines } from './machines';
import { websocketMachines } from './websocketMachines';
import { initWebsocket } from './initWebsocket';
import { updateMachinesWebsocket } from './updateMachinesWebsocket';

export default combineReducers({
  machines,
  websocketMachines,
  initWebsocket,
  updateMachinesWebsocket,
});
