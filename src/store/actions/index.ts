import axios from 'axios';
import { Dispatch, AnyAction } from 'redux';

import {
  FETCH_MACHINES_FAILED,
  FETCH_MACHINES_SUCCESS,
  WEBSOCKET_MACHINES_FETCHED,
  INIT_WEBSOCKET_SUCCESS,
  UPDATE_MACHINES,
  WEBSOCKET_MACHINES_UPDATE,
} from '@src/contants';

interface WebsocketMachinesProps {
  status: string;
  machine_id: string;
  id: string;
  timestamp: Date;
}

const machinesFetched = (machines: MachinesGlobal) => ({
  type: FETCH_MACHINES_SUCCESS,
  machines,
});

const machinesNotFetched = (machines: MachinesGlobal) => ({
  type: FETCH_MACHINES_FAILED,
  machines,
});

export const websocketMachinesFetched = (
  websocketMachines: WebsocketMachinesProps,
): AnyAction => ({
  type: WEBSOCKET_MACHINES_FETCHED,
  websocketMachines,
});

export const updateMachinesThunk = (data: MachinesGlobal): AnyAction => ({
  type: UPDATE_MACHINES,
  data,
});

export const fetchMachines = () => {
  return (dispatch: Dispatch): void => {
    axios
      .get<MachinesGlobal>(
        'https://machinestream.herokuapp.com/api/v1/machines',
      )
      .then((res) => res.data)
      .then((data) => dispatch(machinesFetched(data)))
      .catch((err) => {
        dispatch(machinesNotFetched(err));
        console.log(err);
      });
  };
};

export const updateFetchMachines = () => {
  return (dispatch: Dispatch, getState: () => MachinesGlobal): void => {
    dispatch(updateMachinesThunk(getState()));
  };
};

export const initWebsocket = (status: boolean): AnyAction => ({
  type: INIT_WEBSOCKET_SUCCESS,
  status,
});

export const updateMachinesWebsocket = (
  websocketMachines: WebsocketMachinesProps,
): AnyAction => ({
  type: WEBSOCKET_MACHINES_UPDATE,
  websocketMachines,
});

export const updateFetchMachinesWebsocket = () => {
  return (dispatch: Dispatch, getState: () => WebsocketMachinesProps): void => {
    dispatch(updateMachinesWebsocket(getState()));
  };
};
