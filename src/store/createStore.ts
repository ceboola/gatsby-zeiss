import { createStore as reduxCreateStore, applyMiddleware, Store } from 'redux';
import thunk from 'redux-thunk';

import reducer from './reducers';

const createStore = (): Store =>
  reduxCreateStore(reducer, applyMiddleware(thunk));

export default createStore;
