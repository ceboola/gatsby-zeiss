import { memo } from 'react';
import Button from '@material-ui/core/Button';

import Seo from '@src/components/Seo';
import Machines from '@components/Machines';
import { fetchMachines } from '@src/store/actions';
import { useDispatch } from 'react-redux';

const IndexPage = () => {
  const dispatch = useDispatch();
  const fetchNewData = () => {
    dispatch(fetchMachines());
  };

  return (
    <>
      <Seo title="All machines" />
      <Button variant="contained" color="secondary" onClick={fetchNewData}>
        fetch new static data
      </Button>
      <h1>All machines</h1>
      <Machines />
    </>
  );
};

export default memo(IndexPage);
