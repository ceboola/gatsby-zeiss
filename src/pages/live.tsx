/* eslint-disable @typescript-eslint/ban-ts-comment */
import { useSelector } from 'react-redux';
import { memo } from 'react';

import Seo from '@src/components/Seo';
import Terminal from '@components/Terminal';

const LivePage = () => {
  const liveData = useSelector(
    (state: { updateMachinesWebsocket: WebsocketMachinesGlobal }) =>
      state.updateMachinesWebsocket,
  );

  return (
    <>
      <Seo title="Live Data" />
      <h1>All live data</h1>
      {/* @ts-ignore */}
      <Terminal data={liveData} />
    </>
  );
};

export default memo(LivePage);
