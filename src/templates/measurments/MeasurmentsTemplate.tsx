import { VFC } from 'react';
import MachineDetails from '@components/MachineDetails';
import Seo from '@src/components/Seo';

import { MeasurmentsTemplateProps } from './types';

const MeasurmentsTemplate: VFC<MeasurmentsTemplateProps> = ({
  pageContext,
}) => {
  return (
    <>
      <Seo title="Measurements" />
      <MachineDetails data={pageContext} />
    </>
  );
};

export default MeasurmentsTemplate;
