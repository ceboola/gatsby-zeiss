import { VFC } from 'react';
import MachineDetails from '@components/MachineDetails';
import Seo from '@src/components/Seo';

import { MicroscopesTemplateProps } from './types';

const MicroscopesTemplate: VFC<MicroscopesTemplateProps> = ({
  pageContext,
}) => {
  return (
    <>
      <Seo title="Microscopes" />
      <MachineDetails data={pageContext} />
    </>
  );
};

export default MicroscopesTemplate;
