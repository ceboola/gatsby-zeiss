import { memo, VFC } from 'react';

import { StyledSvg } from './styled';
import type { CommonIconProps } from './types';

const reqSvgs = require.context(
  '!!svg-sprite-loader!../../images',
  false,
  /\.svg$/,
);

const svgs = reqSvgs
  .keys()
  .reduce(
    (images: Record<string, { id: string; viewBox: string }>, path: string) => {
      const svg = reqSvgs(path).default;
      images[svg.id] = svg;
      return images;
    },
    {},
  );

const Icon: VFC<CommonIconProps> = ({
  name,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  viewBox: _,
  width,
  height,
  ...otherProps
}) => {
  const { id, viewBox } = svgs[name];

  return (
    <StyledSvg width={width} height={height} viewBox={viewBox} {...otherProps}>
      <use xlinkHref={`#${id}`} />
    </StyledSvg>
  );
};

export default memo(Icon);
