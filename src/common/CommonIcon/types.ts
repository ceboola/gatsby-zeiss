export interface CommonIconProps {
  name: string;
  viewBox?: string;
  width?: number | string;
  height?: number | string;
}
