import styled from 'styled-components';
import { Link } from 'gatsby';

export const Menu = styled.div`
  margin-left: auto;
`;

export const StyledLink = styled(Link)`
  box-shadow: none;
  color: unset;
  text-decoration: none;
`;

export const WebSocket = styled.div`
  display: flex;
  align-items: center;
  margin-left: 20px;
  & svg {
    margin-left: 8px;
  }
`;
