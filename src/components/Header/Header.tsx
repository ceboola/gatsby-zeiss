import { memo, VFC } from 'react';
import { useSelector } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Slide from '@material-ui/core/Slide';

import CommonIcon from '@common/CommonIcon';

import { HeaderProps, HideOnScrollProps, InitWebsocketProps } from './types';
import { Menu, StyledLink, WebSocket } from './styled';

const Header: VFC<HeaderProps> = ({ siteTitle }, props) => {
  const isInitWebsocket = useSelector(
    (state: InitWebsocketProps) => state.initWebsocket.status,
  );

  const HideOnScroll = (props: HideOnScrollProps) => {
    const { children, window } = props;
    const trigger = useScrollTrigger({ target: window ? window() : undefined });

    return (
      <Slide appear={false} direction="down" in={!trigger}>
        {children}
      </Slide>
    );
  };

  return (
    <div>
      <CssBaseline />
      <HideOnScroll {...props}>
        <AppBar>
          <Toolbar>
            <Typography variant="h6">
              <StyledLink to="/">{siteTitle}</StyledLink>
            </Typography>
            <WebSocket>
              websocket:
              <CommonIcon
                name={isInitWebsocket ? 'switch-on' : 'switch-off'}
                width={26}
                height={26}
              />
            </WebSocket>
            <Menu>
              <Button color="inherit">
                <StyledLink to="/live/">LiveData</StyledLink>
              </Button>
            </Menu>
          </Toolbar>
        </AppBar>
      </HideOnScroll>
      <Toolbar />
    </div>
  );
};

export default memo(Header);
