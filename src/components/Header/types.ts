import { ReactElement } from 'react';

export interface HeaderProps {
  siteTitle: string;
}

export interface HideOnScrollProps {
  children: ReactElement;
  window: () => Window;
}

export interface InitWebsocketProps {
  initWebsocket: {
    status: string;
  };
}
