import styled from 'styled-components';

const buttonSize = {
  width: '12px',
  height: '12px',
  borderRadius: '100%',
  display: 'block',
};

export const Window = styled.div`
  width: 100%;
  margin: 0 auto 2rem;
  box-shadow: 0 0.25rem 0.5rem #12181e;
  border-radius: 0.5rem 0.5rem 0 0;
`;

export const Bar = styled.div`
  background: #3f51b5;
  height: 36px;
  border-radius: 0.5rem 0.5rem 0 0;
`;

export const Indicator = styled.div`
  background: #f6b73e;
  position: relative;
  margin-left: 38px;
  top: 12px;
  ${buttonSize}
  &::after,
  ::before {
    content: ' ';
    position: absolute;
    ${buttonSize}
  }
  &::before {
    background: #f55551;
    margin-left: -20px;
  }
  &::after {
    background: #32c146;
    margin-left: 20px;
  }
`;

export const Title = styled.h2`
  font-size: 12px;
  color: #fff;
  margin-left: auto;
  margin: 0;
  display: flex;
  justify-content: flex-end;
  margin-right: 18px;
`;
export const Area = styled.div`
  background: #232323;
  padding: 18px;
  font-size: 11px;
  color: #32c146;
`;

const handleColorType = (statusColor: string) => {
  switch (statusColor) {
    case 'running':
      return 'green';
    case 'finished':
      return 'yellow';
    case 'repaired':
      return 'gray';
    case 'errored':
      return 'red';
    default:
      return '#fff';
  }
};

export const Info = styled.div<{ statusColor: string }>`
  display: flex;
  flex-direction: row;
  margin: 4px 0;
  color: ${(props) => props.statusColor && handleColorType(props.statusColor)};
  & > div {
    margin: 0 10px;
  }
`;
