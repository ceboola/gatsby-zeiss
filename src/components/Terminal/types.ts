export interface TerminalProps {
  data: [
    {
      timestamp: Date;
      status: string;
      machine_id?: string;
      id?: string;
    },
  ];
}
