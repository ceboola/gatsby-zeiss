import { VFC } from 'react';
import dayjs from 'dayjs';

import { Window, Bar, Indicator, Area, Title, Info } from './styled';
import { TerminalProps } from './types';

const Terminal: VFC<TerminalProps> = ({ data }) => {
  return (
    <Window>
      <Bar>
        <Indicator />
        <Title>Log events</Title>
      </Bar>
      <Area>
        {data?.map((event) => (
          <Info statusColor={event.status}>
            <div>{dayjs(event.timestamp).format('YYYY-MM-DD HH:mm')}</div>
            <div>{event.status}</div>
            {event.machine_id && <div>{event.machine_id}</div>}
          </Info>
        ))}
      </Area>
    </Window>
  );
};

export default Terminal;
