import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 12px 0;
`;

export const Title = styled.span`
  font-size: 12px;
  font-weight: 700;
  margin: 4px 0;
`;

export const Description = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 12px 12px 12px;
  font-size: 13px;
  > div {
    margin: 2px 0;
  }
`;

export const Identity = styled.div`
  font-size: 10px;
`;

export const Status = styled.div`
  display: flex;
  align-items: center;
  width: max-content;
  cursor: pointer;
  & svg {
    margin-left: 5px;
  }
`;
