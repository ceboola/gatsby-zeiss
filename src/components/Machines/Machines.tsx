import { memo, useState, VFC } from 'react';
import { useSelector } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';

import SingleMachine from '@components/SingleMachine';
import LearnMore from '@components/LearnMore';

import { MachinesProps } from './types';

const Machines: VFC = () => {
  const [filter, setFilter] = useState('all');
  const filteredMachines = useSelector((state: MachinesProps) => {
    const machines = state.machines.data;
    if (filter === 'all') {
      return machines;
    }
    return machines.filter((machine) => machine.machine_type === filter);
  });

  const handleChange = (e: React.FormEvent<HTMLSelectElement>) => {
    const target = e.target as HTMLSelectElement;
    setFilter(target.value);
  };

  return (
    <div>
      <label htmlFor="machines-select">Choose a machine type:</label>
      <select name="machines" id="machines-select" onChange={handleChange}>
        <option value="all">All</option>
        <option value="microscope">Microscope</option>
        <option value="measurement">Measurement</option>
      </select>
      {filteredMachines.length > 0 ? (
        <Grid container spacing={2}>
          {filteredMachines.map((machine) => (
            <Grid key={machine.id} item xs={6} sm={4} md={3} xl={2}>
              <SingleMachine id={machine.id}>
                <LearnMore link={`/${machine.machine_type}/${machine.id}/`} />
              </SingleMachine>
            </Grid>
          ))}
        </Grid>
      ) : (
        <CircularProgress />
      )}
    </div>
  );
};

export default memo(Machines);
