import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 0 auto;
  margin-top: 40px;
  max-width: 960px;
  padding: 0 1.0875rem 1.45rem;
`;
