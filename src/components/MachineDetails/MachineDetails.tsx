import { memo, useEffect, useState, VFC } from 'react';
import axios from 'axios';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Terminal from '@components/Terminal';
import SingleMachine from '@components/SingleMachine';

import { MachineDetailsProps } from './types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      margin: 'auto',
      maxWidth: '100%',
    },
    image: {
      width: 256,
      height: 256,
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%',
    },
  }),
);

const MachineDetails: VFC<MachineDetailsProps> = ({ data }) => {
  const [fetchData, setFetchData] = useState(
    ([] as unknown) as {
      data: { events: [{ timestamp: Date; status: string }] };
    },
  );
  const classes = useStyles();
  useEffect(() => {
    (async () => {
      try {
        const response = await axios.get(
          `https://machinestream.herokuapp.com/api/v1/machines/${data.machine.id}`,
        );
        setFetchData(response.data);
      } catch {}
    })();
  }, []);

  const { events } = fetchData.data || {};
  return (
    <div>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item>
              <SingleMachine id={data.machine.id} />
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs={12} container direction="column" spacing={2}>
                <Grid item xs>
                  <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <Terminal data={events} />
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </div>
  );
};

export default memo(MachineDetails);
