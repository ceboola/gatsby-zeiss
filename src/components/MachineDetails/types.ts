export interface MachineDetailsProps {
  data: {
    machine: {
      id: string;
    };
  };
}
