import styled, { css, keyframes } from 'styled-components';

const liveCircle = keyframes`
0% {
    opacity: 1;
  }
  25% {
    opacity: 1;
  }
  50% {
    opacity: 0.5;
    box-shadow: 0 0 0px 0px green;
  }
  75% {
    opacity: 1;
  }
  100% {
    opacity: 1;
    box-shadow: 0 0 0px 1rem rgba(255, 0, 0, 0);
  }
`;
export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  flex-direction: column;
`;

export const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 12px 0;
`;

export const Title = styled.span`
  font-size: 12px;
  font-weight: 700;
  margin: 4px 0;
`;

export const Description = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 12px 12px 12px;
  font-size: 13px;
  > div {
    margin: 2px 0;
  }
`;

export const LiveUpdate = styled.div`
  font-size: 12px;
  margin: 4px 0;
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const LiveIndicator = styled.div<{ isLive: boolean }>`
  color: ${(props) => (props.isLive ? 'green' : 'red')};
  text-transform: uppercase;
  font-size: 0.425rem;
  line-height: 1rem;
  display: inline-block;
  padding: 10px;
  position: relative;
  &::before {
    content: '';
    width: 0.3rem;
    height: 0.3rem;
    background: ${(props) => (props.isLive ? 'green' : 'red')};
    display: block;
    position: absolute;
    top: 0.3rem;
    left: 0.3rem;
    border-radius: 100%;
    animation: ${(props) =>
      props.isLive
        ? css`
            ${liveCircle} 4s infinite;
          `
        : 'unset'};
    z-index: 50;
  }
`;

export const Identity = styled.div`
  font-size: 10px;
`;

export const Status = styled.div`
  display: flex;
  align-items: center;
  width: max-content;
  cursor: pointer;
  & svg {
    margin-left: 5px;
  }
`;

export const LearnMore = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 12px;
  & a {
    box-shadow: none;
    text-decoration: none;
  }
`;
