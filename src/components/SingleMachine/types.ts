export interface MachineProps {
  machines: {
    data: MachinesGlobal[];
  };
}

export interface SingleMachineProps {
  id: string;
  children?: React.ReactNode;
}
