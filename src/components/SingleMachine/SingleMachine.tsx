import { memo } from 'react';
import { useSelector } from 'react-redux';
import Card from '@material-ui/core/Card';
import Tooltip from '@material-ui/core/Tooltip';
import dayjs from 'dayjs';

import CommonIcon from '@common/CommonIcon';

import {
  Wrapper,
  Title,
  Header,
  Description,
  Status,
  Identity,
  LiveUpdate,
  LiveIndicator,
} from './styled';
import { SingleMachineProps, MachineProps } from './types';

const SingleMachine: React.FC<SingleMachineProps> = ({ id, children }) => {
  const machine = useSelector((state: MachineProps) =>
    state.machines?.data.find((machine) => machine.id === id),
  );
  if (!machine) return null;
  return (
    <Card>
      <Wrapper>
        <Header>
          <LiveUpdate>
            <LiveIndicator isLive={Number(machine.timestamp) !== 0} />
            live update:{' '}
            {Number(machine.timestamp) !== 0
              ? dayjs(machine.timestamp).format('YYYY-MM-DD HH:mm')
              : 'not yet'}
          </LiveUpdate>
          <Identity>id: {machine.id}</Identity>
          <Title>{machine.machine_type}</Title>
          <CommonIcon name={machine.machine_type} width={54} height={54} />
        </Header>
        <Description>
          <Tooltip title={machine.status} placement="right-start">
            <Status>
              <b>status</b>:
              <CommonIcon name={machine.status} width={16} height={16} />
            </Status>
          </Tooltip>
          <div>
            <b>installed</b>: {machine.install_date}
          </div>
          <div>
            <b>maintenance</b>:{' '}
            {dayjs(machine.last_maintenance).format('YYYY-MM-DD HH:mm')}
          </div>
          <div>
            <b>floor</b>: {machine.floor}
          </div>
        </Description>
      </Wrapper>
      {children}
    </Card>
  );
};

export default memo(SingleMachine);
