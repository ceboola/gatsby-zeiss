import { VFC } from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'gatsby';

import { Wrapper } from './styled';
import { LearnMoreProps } from './types';

const LearnMore: VFC<LearnMoreProps> = ({ link }) => {
  return (
    <Wrapper>
      <Link to={link}>
        <Button variant="contained" color="primary" size="small">
          Learn More
        </Button>
      </Link>
    </Wrapper>
  );
};

export default LearnMore;
