import { ReactElement, useEffect, VFC } from 'react';
import { Provider, useDispatch, useSelector } from 'react-redux';
import { Socket } from 'phoenix';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/core/styles';

import theme from '@src/theme';
import Layout from '@components/Layout';
import createStore from '@store/createStore';
import {
  fetchMachines,
  websocketMachinesFetched,
  initWebsocket,
  updateFetchMachines,
  updateMachinesWebsocket,
} from '@store/actions';

const BaseElement: VFC<{ element: ReactElement }> = ({ element }) => {
  const dispatch = useDispatch();
  const isInitWebsocket = useSelector(
    (state: { initWebsocket: { status: boolean } }) =>
      state.initWebsocket.status,
  );

  useEffect(() => {
    dispatch(fetchMachines());
  }, []);

  let socket;
  useEffect(() => {
    if (!isInitWebsocket) {
      socket = new Socket('wss://machinestream.herokuapp.com/api/v1/events');
      socket.connect();
      socket.onOpen = () => dispatch(initWebsocket(true));
      socket.onError = () => dispatch(initWebsocket(false));
      const channel = socket.channel('events', {});
      channel.join();
      channel.on('new', (event: WebsocketMachinesGlobal) => {
        dispatch(websocketMachinesFetched(event));
        dispatch(updateFetchMachines());
        dispatch(updateMachinesWebsocket(event));
      });
    }
  }, []);
  return <>{element}</>;
};

const TopLayout: VFC<{ element: ReactElement }> = ({ element }) => {
  const store = createStore();
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <Layout>
          <BaseElement element={element} />
        </Layout>
      </ThemeProvider>
    </Provider>
  );
};

export default TopLayout;
