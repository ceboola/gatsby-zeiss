module.exports = {
  siteMetadata: {
    title: `Gatsby zeiss`,
    description: `test case`,
    author: 'ceboola',
  },
  plugins: [
    'gatsby-plugin-top-layout',
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby zeiss`,
        short_name: `zeiss`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-material-ui`,
      options: {
        stylesProvider: {
          injectFirst: true,
        },
      },
    },
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-tsconfig-paths`,
    {
      resolve: `gatsby-plugin-svg-sprite-loader`,
      options: {
        esModule: false,
        publicPath: '/',
        // spriteFilename: 'sprites.svg',
        extract: false,
        symbolId: '[name]',
        /* SVG sprite loader options */
        pluginOptions: {
          /* SVG sprite loader plugin options */
          // plainSprite: true,
        },
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
};
