const axios = require(`axios`);
const path = require(`path`);

exports.createPages = async ({ actions }) => {
  const { createPage } = actions;
  const microscopeTemplate = path.resolve(
    `./src/templates/microscopes/MicroscopesTemplate.tsx`,
  );
  const measurmentTemplate = path.resolve(
    `./src/templates/measurments/MeasurmentsTemplate.tsx`,
  );
  const fetchedMachines = () => {
    return axios
      .get('https://machinestream.herokuapp.com/api/v1/machines')
      .then((res) => res.data)
      .then((data) => data)
      .catch((err) => {
        console.log(err);
      });
  };
  const fetchedData = await fetchedMachines();
  const microscopes = fetchedData.data.filter(
    (value) => value.machine_type === 'microscope',
  );
  const measurments = fetchedData.data.filter(
    (value) => value.machine_type === 'measurement',
  );

  microscopes.forEach((machine) => {
    createPage({
      path: `/${machine.machine_type}/${machine.id}`,
      component: microscopeTemplate,
      context: { machine },
    });
  });

  measurments.forEach((machine) => {
    createPage({
      path: `/${machine.machine_type}/${machine.id}`,
      component: measurmentTemplate,
      context: { machine },
    });
  });
};

exports.onCreateBabelConfig = ({ actions }) => {
  actions.setBabelPlugin({
    name: '@babel/plugin-transform-react-jsx',
    options: {
      runtime: 'automatic',
    },
  });
};
