# stage1 as builder
FROM node:15.5.1-alpine3.12 as builder

# Copy source files
WORKDIR /app 
COPY . ./

# Build prod
RUN rm -rf node_modules yarn.lock
RUN apk --no-cache add shadow \
    gcc \
    musl-dev \
    autoconf \
    automake \
    make \
    libtool \
    nasm \
    tiff \
    jpeg \
    zlib \
    zlib-dev \
    file \
    pkgconf \
    && yarn install
# well known problem https://github.com/gatsbyjs/gatsby/issues/24902
RUN yarn gatsby clean && yarn build

# ----------------------------------
# Prepare production environment
FROM nginx:alpine
# ----------------------------------

# Clean nginx
RUN rm -rf /usr/share/nginx/html/*

# Copy dist
COPY --from=builder /app/public /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html

# Permission
RUN chown root /usr/share/nginx/html/*
RUN chmod 755 /usr/share/nginx/html/*

# Expose port
EXPOSE 5000

# Start
CMD ["nginx", "-g", "daemon off;"]
